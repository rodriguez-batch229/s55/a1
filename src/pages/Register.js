import { Form, Button } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import { Navigate, Link, useNavigate } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Register() {
   const navigate = useNavigate();
   // State hooks -> Store values of the input fields

   const [firstName, setFirstName] = useState("");
   const [lastName, setLastName] = useState("");
   const [email, setEmail] = useState("");
   const [mobileNo, setMobileNo] = useState("");
   const [password1, setPassword1] = useState("");
   const [password2, setPassword2] = useState("");

   const [isActive, setIsActive] = useState(false);

   const { user, setUser } = useContext(UserContext);

   console.log(firstName);
   console.log(lastName);
   console.log(email);
   console.log(mobileNo);
   console.log(password1);
   console.log(password2);

   useEffect(() => {
      // validation to enable register button when all fields is populated and both password an match
      if (
         firstName !== "" &&
         lastName !== "" &&
         email !== "" &&
         mobileNo.length == 11 &&
         password1 !== "" &&
         password2 !== "" &&
         password1 === password2
      ) {
         setIsActive(true);
      } else {
         setIsActive(false);
      }
   }, [firstName, lastName, email, mobileNo, password1, password2]);

   // function to simulate user registration
   function registerUser(e) {
      // prevents page redirection via form submission
      e.preventDefault();
      fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
         method: "POST",
         headers: {
            "Content-Type": "application/json",
         },
         body: JSON.stringify({
            email: email,
         }),
      })
         .then((res) => res.json())
         .then((data) => {
            console.log(data);
            if (data) {
               Swal.fire({
                  title: `Failed to Register`,
                  icon: "error",
                  text: `${email} already registered`,
               });
            } else {
               fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                  method: "POST",
                  headers: {
                     "Content-Type": "application/json",
                  },
                  body: JSON.stringify({
                     firstName: firstName,
                     lastName: lastName,
                     email: email,
                     mobileNo: mobileNo,
                     password: password1,
                  }),
               })
                  .then((res) => res.json())
                  .then((data) => {
                     // If no user information is found, the "access" property will not be available and will return undefined

                     console.log(data);

                     if (data) {
                        Swal.fire({
                           title: "Successfully Registered!",
                           icon: "success",
                        });
                        setFirstName("");
                        setLastName("");
                        setEmail("");
                        setMobileNo("");
                        setPassword1("");
                        setPassword2("");
                        navigate("/login");
                     } else {
                        Swal.fire({
                           title: "Failed to Register",
                           icon: "error",
                           text: "Please try again",
                        });
                     }
                  });
            }
         });
   }

   return user.id !== null ? (
      <Navigate to="/courses" />
   ) : (
      <Form onSubmit={(e) => registerUser(e)}>
         <h1 className="text-center">Register</h1>{" "}
         <Form.Group className="mb-3" controlId="firstName">
            <Form.Label>First Name</Form.Label>
            <Form.Control
               type="text"
               placeholder="Enter first name"
               value={firstName}
               onChange={(event) => setFirstName(event.target.value)}
               required
            />
         </Form.Group>
         <Form.Group className="mb-3" controlId="lastName">
            <Form.Label>Last Name</Form.Label>
            <Form.Control
               type="text"
               placeholder="Enter last name"
               value={lastName}
               onChange={(event) => setLastName(event.target.value)}
               required
            />
         </Form.Group>
         <Form.Group className="mb-3" controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
               type="email"
               placeholder="Enter email"
               value={email}
               onChange={(event) => setEmail(event.target.value)}
               required
            />
            <Form.Text className="text-muted">
               We'll never share your email with anyone else.
            </Form.Text>
         </Form.Group>
         <Form.Group className="mb-3" controlId="mobileNo">
            <Form.Label>Mobile Number</Form.Label>
            <Form.Control
               type="number"
               placeholder="Enter mobile number"
               value={mobileNo}
               onChange={(event) => setMobileNo(event.target.value)}
               required
            />
         </Form.Group>
         <Form.Group className="mb-3" controlId="password1">
            <Form.Label>Password</Form.Label>
            <Form.Control
               type="password"
               placeholder="Password"
               value={password1}
               onChange={(event) => setPassword1(event.target.value)}
               required
            />
         </Form.Group>
         <Form.Group className="mb-3" controlId="password2">
            <Form.Label>Verify Password</Form.Label>
            <Form.Control
               type="password"
               placeholder="Password"
               value={password2}
               onChange={(event) => setPassword2(event.target.value)}
               required
            />
         </Form.Group>
         {/* Conditional Rendering -> if active button is clickable -> if inactive button is not clickable */}
         {isActive ? (
            <Button variant="primary" type="submit" controlId="submitBtn">
               Register
            </Button>
         ) : (
            <Button
               variant="primary"
               type="submit"
               controlId="submitBtn"
               disabled
            >
               Register
            </Button>
         )}
      </Form>
   );
}
